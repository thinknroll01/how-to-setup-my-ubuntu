# README #

This guild will guide you how to install Ubuntu with gnome-shell, zsh, customized vi and full thai-font support.

# Let's begin

## Install Ubuntu with gnome-shell

First of all we need Ubuntu-server (I choose this because I want my Ubuntu to be as LITE as possible).

Ubuntu-server can be download from [here](http://www.ubuntu.com/download/server).

After Ubuntu installed, let'u update over package first


```
sudo apt-get update && sudo apt-get upgrade -y
```

Now we install gnome-shell for our desktop.

```
sudo apt-get install gnome-core xorg
```

And I need to install font for my language too.

```
sudo apt-get install fonts-thai-tlwg
```

also, I like Ubuntu fonts more than default from gnome.

```
sudo apt-get install ttf-ubuntu-font-family fonts-ubuntu-font-family-console
```

Then, we need tools for customize our fonts.

```
sudo apt-get install gnome-tweak-tool
```

One paste
```
sudo apt-get install gnome-core xorg fonts-thai-tlwg ttf-ubuntu-font-family \
fonts-ubuntu-font-family-console gnome-tweak-tool
```

If you want to use network-manager to manage your interface you have to remove interface configure from `/etc/network/interface`
```
sudo vi /etc/network/interface
``` 
then remove lines with your network interface.

After you finished, restart our Ubuntu

```
sudo reboot
```


After reboot Ubuntu should startup with gnome-shell. 
Login and open tweak tool
go to Fonts menu, configure font as you like. For me, I use `Ubuntu Medium` for `Title` and `Ubuntu` for `Interface`. Then, set `Hinting` to `slight` and `Antialiasing` to `Rgba`

## Customize VI

First check that `vim` have been installed

```
sudo apt-get install vim vim-gtk
```

Clone this repository to your machine.  

```
cd how-to-setup-my-ubuntu/vi
sudo ./vim_custom.sh
```

Now, our customized vim is ready

## Install z-shell

Install zsh.
```
sudo apt-get install zsh
```
Make zsh default shell for your account.
```
chsh -s /usr/bin/zsh
```
After this next time you login you will get zsh as you default.
Customize zsh
```
cd how-to-setup-my-ubuntu/zsh
cp zshrc ~/.zshrc
curl -L https://raw.githubusercontent.com/zsh-users/antigen/master/antigen.zsh > antigen.zsh
mv antigen.zsh ~/.antigen.zsh
```

Open ~/.zshrc and change `DEFAULT_USER` to your username
```
vi ~/.zshrc
```

Now open `zsh` and wait.
```
zsh
```
After `zsh` is ready, `reboot` you machine again

Congratulation, now you have my customized Ubuntu!

# More Configurations


### Alt windows control

If you want to use `Alt` for windows control just like Ubuntu instead of gnome `windows key`.
You can change it in *tweak tool* under **Windows > Windows Action Key**

### Change Caps Lock to Ctrl

You can also change it in *tweak tool* under **Typing > Caps Lock key behaviors** select *Make Caps Lock an additional Ctrl*

# Credits
theme.vim from [Lokaltog](https://github.com/Lokaltog/vim-distinguished)